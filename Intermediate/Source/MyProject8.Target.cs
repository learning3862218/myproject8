using UnrealBuildTool;

public class MyProject8Target : TargetRules
{
	public MyProject8Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("MyProject8");
	}
}
